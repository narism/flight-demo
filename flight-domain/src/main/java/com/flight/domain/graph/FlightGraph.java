package com.flight.domain.graph;

import java.util.HashSet;
import java.util.Set;

public class FlightGraph {
  private Set<Airport> airports;
  private boolean directed;

  public FlightGraph(boolean directed) {
    this.directed = directed;
    this.airports = new HashSet<>();
  }

  public Set<Airport> getAirports() {
    return airports;
  }

  public void setAirports(Set<Airport> airports) {
    this.airports = airports;
  }

  public boolean isDirected() {
    return directed;
  }

  public void setDirected(boolean directed) {
    this.directed = directed;
  }
}