package com.flight.domain.graph;

import java.util.HashSet;
import java.util.Set;

public class Airport {
  private String airportId;
  private String name;
  private String iata;
  private String icao;
  private Set<FlightRoute> routes;
  private double latitude;
  private double longitude;
  private double altitude;

  public Airport(String airportId, String name, String iata, String icao, double latitude, double longitude, double altitude) {
    this.airportId = airportId;
    this.name = name;
    this.iata = iata;
    this.icao = icao;
    this.routes = new HashSet<>();
    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;
  }

  public String getAirportId() {
    return airportId;
  }

  public void setAirportId(String airportId) {
    this.airportId = airportId;
  }

  public String getIcao() {
    return icao;
  }

  public Set<FlightRoute> getRoutes() {
    return routes;
  }

  public String getIata() {
    return iata;
  }

  public void setIata(String iata) {
    this.iata = iata;
  }
  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public double getAltitude() {
    return altitude;
  }

  public void setAltitude(double altitude) {
    this.altitude = altitude;
  }

  public String getName() {
    return name;
  }
}
