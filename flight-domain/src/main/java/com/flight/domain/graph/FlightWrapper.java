package com.flight.domain.graph;

public class FlightWrapper {
  private Airport sourceAirport;
  private Airport destinationAirport;

  public FlightWrapper(Airport sourceAirport, Airport destinationAirport) {
    this.sourceAirport = sourceAirport;
    this.destinationAirport = destinationAirport;
  }

  public Airport getSourceAirport() {
    return sourceAirport;
  }

  public void setSourceAirport(Airport sourceAirport) {
    this.sourceAirport = sourceAirport;
  }

  public Airport getDestinationAirport() {
    return destinationAirport;
  }

  public void setDestinationAirport(Airport destinationAirport) {
    this.destinationAirport = destinationAirport;
  }
}
