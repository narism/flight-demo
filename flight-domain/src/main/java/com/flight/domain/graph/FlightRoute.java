package com.flight.domain.graph;

import java.util.Objects;

public class FlightRoute implements Comparable<FlightRoute> {

  private Airport source;
  private Airport destination;
  private double distance;

  public FlightRoute(Airport source, Airport destination, double distance) {
    this.source = source;
    this.destination = destination;
    this.distance = distance;
  }

  public Airport getSource() {
    return source;
  }

  public void setSource(Airport source) {
    this.source = source;
  }

  public Airport getDestination() {
    return destination;
  }

  public void setDestination(Airport destination) {
    this.destination = destination;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }

  @Override
  public String toString() {
    return String.format("(%s -> %s, %f)", source.getIcao(), destination.getIcao(), distance);
  }

  @Override
  public int compareTo(FlightRoute otherEdge) {
    if (this.distance > otherEdge.getDistance()) {
      return 1;
    }
    else return -1;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof FlightRoute)) {
      return false;
    }
    FlightRoute flightRoute = (FlightRoute) o;
    return flightRoute.destination.getAirportId().equals(this.destination.getAirportId())
        && flightRoute.source.getAirportId().equals(this.source.getAirportId());
  }

  @Override
  public int hashCode(){
    return Objects.hash(this.destination.getAirportId(), this.source.getAirportId());
  }

}