package com.flight.domain.api;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class TravelRegistrationResponse {
  private UUID travelId;
  private String sourceAirport;
  private String destinationAirport;
  private BigDecimal distance;
  private List<Flight> flights = new LinkedList<>();

  public UUID getTravelId() {
    return travelId;
  }

  public void setTravelId(UUID travelId) {
    this.travelId = travelId;
  }

  public String getSourceAirport() {
    return sourceAirport;
  }

  public void setSourceAirport(String sourceAirport) {
    this.sourceAirport = sourceAirport;
  }

  public String getDestinationAirport() {
    return destinationAirport;
  }

  public void setDestinationAirport(String destinationAirport) {
    this.destinationAirport = destinationAirport;
  }

  public List<Flight> getFlights() {
    return flights;
  }

  public void setFlights(List<Flight> flights) {
    this.flights = flights;
  }

  public BigDecimal getDistance() {
    return distance;
  }

  public void setDistance(BigDecimal distance) {
    this.distance = distance;
  }
}
