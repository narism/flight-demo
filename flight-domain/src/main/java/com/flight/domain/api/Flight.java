package com.flight.domain.api;

public class Flight {
  private String sourceAirport;
  private String destinationAirport;

  public String getSourceAirport() {
    return sourceAirport;
  }

  public void setSourceAirport(String sourceAirport) {
    this.sourceAirport = sourceAirport;
  }

  public String getDestinationAirport() {
    return destinationAirport;
  }

  public void setDestinationAirport(String destinationAirport) {
    this.destinationAirport = destinationAirport;
  }
}
