package com.flight.domain.api;

public class TravelUpdateRequest {
  private String oldAirportId;
  private String newAirportId;

  public String getOldAirportId() {
    return oldAirportId;
  }

  public void setOldAirportId(String oldAirportId) {
    this.oldAirportId = oldAirportId;
  }

  public String getNewAirportId() {
    return newAirportId;
  }

  public void setNewAirportId(String newAirportId) {
    this.newAirportId = newAirportId;
  }
}
