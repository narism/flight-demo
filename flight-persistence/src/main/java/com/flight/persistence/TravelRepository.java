package com.flight.persistence;

import com.flight.domain.api.TravelRegistrationResponse;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class TravelRepository {
  private Map<UUID, TravelRegistrationResponse> travels = new HashMap<>();

  public UUID addTravelRegistrationResponse(TravelRegistrationResponse travelRegistrationResponse) {
    UUID uuid = UUID.randomUUID();
    travels.put(uuid, travelRegistrationResponse);
    return uuid;
  }

  public TravelRegistrationResponse getTravelResponseByUuid(UUID travelId) {
    return travels.get(travelId);
  }

  public void updateTravelRegistrationResponse(TravelRegistrationResponse response) {
    travels.put(response.getTravelId(), response);
  }
}
