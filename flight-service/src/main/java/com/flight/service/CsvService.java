package com.flight.service;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.flight.domain.AirlineRouteData;
import com.flight.domain.AirportData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CsvService {

  @Value("classpath:airport.dat")
  Resource airportFile;
  @Value("classpath:routes.dat")
  Resource routeFile;

  public List<AirportData> readAirportsFromCsv() throws IOException {
    CsvMapper mapper = new CsvMapper();
    mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    CsvSchema schema = mapper.schemaFor(AirportData.class);
    MappingIterator<AirportData> it = mapper.readerFor(AirportData.class).with(schema)
        .readValues(airportFile.getInputStream());
    return it.readAll();
  }

  public List<AirlineRouteData> readRoutesFromCsv() throws IOException {
    CsvMapper mapper = new CsvMapper();
    mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    CsvSchema schema = mapper.schemaFor(AirlineRouteData.class);
    MappingIterator<AirlineRouteData> it = mapper.readerFor(AirlineRouteData.class).with(schema)
        .readValues(routeFile.getInputStream());
    return it.readAll();
  }
}
