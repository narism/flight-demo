package com.flight.service;

import com.flight.domain.AirlineRouteData;
import com.flight.domain.AirportData;
import com.flight.domain.api.Flight;
import com.flight.domain.api.TravelRegistrationResponse;
import com.flight.domain.api.TravelUpdateRequest;
import com.flight.domain.graph.Airport;
import com.flight.domain.graph.FlightGraph;
import com.flight.domain.graph.FlightRoute;
import com.flight.domain.graph.FlightWrapper;
import com.flight.persistence.TravelRepository;
import com.flight.service.exception.BusinessException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static java.util.Objects.isNull;
import static org.springframework.util.StringUtils.isEmpty;

@Service
public class FlightRouteService {

  private CsvService csvService;

  private TravelRepository travelRepository;

  private final FlightGraph flightGraph;


  public FlightRouteService(CsvService csvService, TravelRepository travelRepository) throws IOException {
    this.csvService = csvService;
    this.travelRepository = travelRepository;
    this.flightGraph = generateFlightGraph();
  }

  private FlightGraph generateFlightGraph() throws IOException {
    List<AirlineRouteData> airlineRoutes = csvService.readRoutesFromCsv();
    List<AirportData> airports = csvService.readAirportsFromCsv();
    Map<String, Airport> airportMap = new HashMap<>();
    FlightGraph flightGraph = new FlightGraph(true);
    buildGraphAirports(airports, airportMap, flightGraph);
    buildGraphRoutes(airlineRoutes, airportMap, flightGraph);
    return flightGraph;
  }

  private void buildGraphRoutes(List<AirlineRouteData> airlineRoutes, Map<String, Airport> airportMap, FlightGraph flightGraph) {
    flightGraph.getAirports().stream().forEach(sourceAirport -> airlineRoutes.forEach(airlineRoute -> {
      if (airlineRoute.getSourceId().equals(sourceAirport.getAirportId())) {
        Airport destAirport = airportMap.get(airlineRoute.getDestId());
        if (destAirport != null) {
          FlightRoute flightRoute = new FlightRoute(sourceAirport, destAirport, calcDistance(sourceAirport.getLatitude(), destAirport.getLatitude(), sourceAirport.getLongitude(), destAirport.getLongitude(), sourceAirport.getAltitude(), destAirport.getAltitude()));
          sourceAirport.getRoutes().add(flightRoute);
        }
      }
    }));
  }

  private void buildGraphAirports(List<AirportData> airports, Map<String, Airport> airportMap, FlightGraph flightGraph) {
    airports.forEach(airportData -> {
      Airport airport = new Airport(airportData.getAirPortId(), airportData.getName(), airportData.getIata(), airportData.getIcao(), airportData.getLatitude(), airportData.getLongitude(), airportData.getAltitude());
      flightGraph.getAirports().add(airport);
      airportMap.put(airport.getAirportId(), airport);
    });
  }

  public TravelRegistrationResponse registerTravel(String sourceId, String destinationId) {
    FlightWrapper flightWrapper = getFlight(sourceId, destinationId);
    TravelRegistrationResponse response = findShortestPath(flightWrapper.getSourceAirport(), flightWrapper.getDestinationAirport());
    response.setTravelId(travelRepository.addTravelRegistrationResponse(response));
    return response;
  }

  public double calcDistance(double sourceLatitude, double destLatitude, double sourceLongitude,
                             double destLongitude, double sourceAltitude, double destAltitude) {

    final int earthRadius = 6371; // Radius of the earth
    double latDistance = Math.toRadians(destLatitude - sourceLatitude);
    double lonDistance = Math.toRadians(destLongitude - sourceLongitude);
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
        + Math.cos(Math.toRadians(sourceLatitude)) * Math.cos(Math.toRadians(destLatitude))
        * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = earthRadius * c * 1000;

    double height = sourceAltitude - destAltitude;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance);
  }

  @Cacheable(value = "shortestPath")
  private TravelRegistrationResponse findShortestPath(Airport start, Airport end) {
    TravelRegistrationResponse response = new TravelRegistrationResponse();
    // We keep track of which path gives us the shortest path for each node
    // by keeping track how we arrived at a particular node, we effectively
    // keep a "pointer" to the parent node of each node, and we follow that
    // path to the start
    HashMap<Airport, Airport> changedAt = new HashMap<>();
    changedAt.put(start, null);
    Set<String> visitedAirports = new HashSet<>();
    // Keeps track of the shortest path we've found so far for every node
    HashMap<Airport, Double> shortestPathMap = new HashMap<>();

    // Setting every node's shortest path weight to positive infinity to start
    // except the starting node, whose shortest path weight is 0
    setInitialLenghts(start, shortestPathMap);

    // Now we go through all the nodes we can go to from the starting node
    // (this keeps the loop a bit simpler)
    setCurrentPaths(start, changedAt, shortestPathMap);
    visitedAirports.add(start.getAirportId());

    // This loop runs as long as there is an unvisited node that we can
    // reach from any of the nodes we could till then
    while (true) {
      Airport currentNode = closestReachableUnvisited(shortestPathMap, visitedAirports);
      // If we haven't reached the end node yet, and there isn't another
      // reachable node the path between start and end doesn't exist
      // (they aren't connected)
      if (currentNode == null) {
        throw new BusinessException("Travel route not found");
      }

      if (currentNode == end) {
        response.setSourceAirport(Optional.ofNullable(start.getIata()).orElse(start.getIcao()));
        response.setDestinationAirport(Optional.ofNullable(end.getIata()).orElse(end.getIcao()));

        Airport child = end;

        while (true) {
          Airport parent = changedAt.get(child);
          if (parent == null) {
            break;
          }
          Flight flight = new Flight();
          flight.setDestinationAirport(Optional.ofNullable(child.getIata()).orElse(child.getIcao()));
          flight.setSourceAirport(Optional.ofNullable(parent.getIata()).orElse(parent.getIcao()));
          // Since our changedAt map keeps track of child -> parent relations
          // in order to save the path we need to add the parent before the child and
          // it's descendants
          response.getFlights().add(0, flight);

          child = parent;
        }
        response.setDistance(calcKmDistance(shortestPathMap.get(end)));
        return response;
      }
      visitedAirports.add(currentNode.getAirportId());
      // Now we go through all the unvisited nodes our current node has an edge to
      // and check whether its shortest path value is better when going through our
      // current node than whatever we had before
      for (FlightRoute edge : currentNode.getRoutes()) {
        if (visitedAirports.contains(edge.getDestination().getAirportId()))
          continue;

        if (shortestPathMap.get(currentNode)
            + edge.getDistance()
            < shortestPathMap.get(edge.getDestination())) {
          shortestPathMap.put(edge.getDestination(),
              shortestPathMap.get(currentNode) + edge.getDistance());
          changedAt.put(edge.getDestination(), currentNode);
        }
      }
    }
  }

  private void setCurrentPaths(Airport start, HashMap<Airport, Airport> changedAt, HashMap<Airport, Double> shortestPathMap) {
    for (FlightRoute edge : start.getRoutes()) {
      shortestPathMap.put(edge.getDestination(), edge.getDistance());
      changedAt.put(edge.getDestination(), start);
    }
  }

  private void setInitialLenghts(Airport start, HashMap<Airport, Double> shortestPathMap) {
    for (Airport node : flightGraph.getAirports()) {
      if (node == start)
        shortestPathMap.put(start, 0.0);
      else shortestPathMap.put(node, Double.POSITIVE_INFINITY);
    }
  }

  private BigDecimal calcKmDistance(Double distanceMeters) {
    return new BigDecimal(distanceMeters).divide(BigDecimal.valueOf(1000)).setScale(2, RoundingMode.HALF_UP);
  }


  private Airport closestReachableUnvisited(HashMap<Airport, Double> shortestPathMap, Set<String> visitedAirports) {

    double shortestDistance = Double.POSITIVE_INFINITY;
    Airport closestReachableNode = null;
    for (Airport node : flightGraph.getAirports()) {
      if (visitedAirports.contains(node.getAirportId()))
        continue;

      double currentDistance = shortestPathMap.get(node);
      if (currentDistance == Double.POSITIVE_INFINITY)
        continue;

      if (currentDistance < shortestDistance) {
        shortestDistance = currentDistance;
        closestReachableNode = node;
      }
    }
    return closestReachableNode;
  }

  public TravelRegistrationResponse getTravelById(UUID travelId) {
    TravelRegistrationResponse response = travelRepository.getTravelResponseByUuid(travelId);
    if (isNull(response)) {
      throw new BusinessException("Travel not found");
    }
    return response;
  }

  public TravelRegistrationResponse updateTravel(UUID travelId, TravelUpdateRequest updateRequest) {
    TravelRegistrationResponse response = travelRepository.getTravelResponseByUuid(travelId);
    if (isNull(response)) {
      throw new BusinessException("Travel not found");
    }
    String newAirportId = updateRequest.getNewAirportId();
    String oldAirportId = updateRequest.getOldAirportId();
    validateUpdateRequest(response, newAirportId, oldAirportId);
    FlightWrapper flightWrapper = getFlight(oldAirportId, newAirportId);
    Airport newAirport = flightWrapper.getDestinationAirport();
    Airport oldAirport = flightWrapper.getSourceAirport();
    if (isNull(newAirport) || isNull(oldAirport)) {
      throw new BusinessException("No airports found for input");
    }
    BigDecimal kmDistance = calcKmDistance(calcDistance(newAirport.getLatitude(), oldAirport.getLatitude(), newAirport.getLongitude(), oldAirport.getLongitude(), newAirport.getAltitude(), oldAirport.getAltitude()));
    if (kmDistance.compareTo(new BigDecimal(100)) > 0) {
      throw new BusinessException("Distance between replacement airport too big");
    }
    //Not totally correct. should take into account if there is a flight route and calculate new distances base on that route. But according to spec current approach is fine
    response.setDistance(response.getDistance().add(kmDistance));
    response.getFlights().forEach(flight -> {
      if (flight.getSourceAirport().equals(oldAirportId)) {
        flight.setSourceAirport(newAirportId);
      } else if (flight.getDestinationAirport().equals(oldAirportId)) {
        flight.setDestinationAirport(newAirportId);
      }
    });
    travelRepository.updateTravelRegistrationResponse(response);
    return response;
  }

  private void validateUpdateRequest(TravelRegistrationResponse response, String newAirportId, String oldAirportId) {
    if (isEmpty(newAirportId) || isEmpty(oldAirportId)) {
      throw new BusinessException("Source and destination airport IATA or ICAO has to be provided");
    }
    boolean isOldAirportPresent = response.getFlights().stream().anyMatch(flight -> flight.getDestinationAirport().equals(oldAirportId) || flight.getSourceAirport().equals(oldAirportId));
    if (!isOldAirportPresent) {
      throw new BusinessException("Previous airport not present in travel");
    }
  }

  private FlightWrapper getFlight(String sourceId, String destinationId) {
    Airport source = null;
    Airport dest = null;
    //to not to iterate over airports twice for finding source and destination by IATA/ICAO
    for (Airport airport : flightGraph.getAirports()) {
      if (source != null && dest != null) {
        break;
      }
      if (source == null && airport.getIata().equals(sourceId) || airport.getIcao().equals(sourceId)) {
        source = airport;
      }
      if (dest == null && airport.getIata().equals(destinationId) || airport.getIcao().equals(destinationId)) {
        dest = airport;
      }
    }
    if (isNull(source) || isNull(dest)) {
      throw new BusinessException("No airport found for input");
    }
    return new FlightWrapper(source, dest);
  }
}
