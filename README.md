**Flight demo app**

*Prerequisites*

JAVA 12+

*Approach*

Airport data and flight route data was taken from
https://openflights.org/data.html

Project has two data files in resources:
_airport.dat_ and _routes.dat_

On application startup those files are used to create an in memory immutable flight route graph with airports 
and distances between those airports based on geographical coordinates (latitude, longitude) and altitude. 
On new travel registration request (POST /travels) source airport identificator and destination airport
identificator has to be provided and with those dijkstras algorithm is used to find smallest path between those
airports. On success this path is also cached. Travel is registered in HashMap instead of real database for simplified approach.
For simplicity reasons project only has integration tests that test the full flow from controller. Also for 
simplicity reasons optimizations like PriorityQueue etc are not used.

**Flow example**

Travel from Tallinn TLL to Toronto Lester B. Pearson International Airport YYZ

POST http://localhost:8080/travels
```javascript
{
	"sourceId":"TLL",
	"destinationId": "YYZ"
}
```

response
```javascript
{
    "travelId": "3cd76a79-9d07-437e-b623-26c4f2f3356f",
    "sourceAirport": "TLL",
    "destinationAirport": "YYZ",
    "distance": 6666.11,
    "flights": [
        {
            "sourceAirport": "TLL",
            "destinationAirport": "TRD"
        },
        {
            "sourceAirport": "TRD",
            "destinationAirport": "KEF"
        },
        {
            "sourceAirport": "KEF",
            "destinationAirport": "YYZ"
        }
    ]
}
```
GET http://localhost:8080/travels/3cd76a79-9d07-437e-b623-26c4f2f3356f
```javascript
{
    "travelId": "3cd76a79-9d07-437e-b623-26c4f2f3356f",
    "sourceAirport": "TLL",
    "destinationAirport": "YYZ",
    "distance": 6666.11,
    "flights": [
        {
            "sourceAirport": "TLL",
            "destinationAirport": "TRD"
        },
        {
            "sourceAirport": "TRD",
            "destinationAirport": "KEF"
        },
        {
            "sourceAirport": "KEF",
            "destinationAirport": "YYZ"
        }
    ]
}
```
To change an airport in a travel (Replace Keflavík(KEF) airport with Reykjavík RKV)

PATCH http://localhost:8080/travels/3cd76a79-9d07-437e-b623-26c4f2f3356f
```javascript
{
	"oldAirportId":"KEF",
	"newAirportId": "RKV"
}
```

Response

```javascript
{
    "travelId": "0d95a6a8-bf4e-4478-b8a3-277a6e4acd4f",
    "sourceAirport": "TLL",
    "destinationAirport": "YYZ",
    "distance": 6702.25,
    "flights": [
        {
            "sourceAirport": "TLL",
            "destinationAirport": "TRD"
        },
        {
            "sourceAirport": "TRD",
            "destinationAirport": "RKV"
        },
        {
            "sourceAirport": "RKV",
            "destinationAirport": "YYZ"
        }
    ]
}
```

**Running the application**
clone from https://narism@bitbucket.org/narism/flight-demo.git


In project root

*gradlew clean build*

navigate to flight-web/build/libs

java -jar flight-web.jar

application should be running in localhost:8080

NB! deployment takes about 30 sec as the graph is built in memory


