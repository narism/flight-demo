package com.flight.web;


import com.flight.domain.api.TravelRegistrationRequest;
import com.flight.domain.api.TravelRegistrationResponse;
import com.flight.domain.api.TravelUpdateRequest;
import com.flight.service.exception.BusinessException;
import com.flight.web.controller.FlightController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightApp.class)
public class FlightControllerTest {

  @Autowired
  private FlightController flightController;

  @Test
  public void noLayoverTravelHasOneFlight() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("GKA");
    request.setDestinationId("MAG");
    TravelRegistrationResponse response = flightController.registerTravel(request);

    assertEquals(1, response.getFlights().size());
    assertEquals("GKA", response.getFlights().get(0).getSourceAirport());
    assertEquals("MAG", response.getFlights().get(0).getDestinationAirport());
  }

  @Test
  public void multipleLayoverFlightsContainMultipleFlights() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("MAG");
    request.setDestinationId("LAE");
    TravelRegistrationResponse response = flightController.registerTravel(request);

    assertEquals(2, response.getFlights().size());
    assertEquals("MAG", response.getFlights().get(0).getSourceAirport());
    assertEquals("HGU", response.getFlights().get(0).getDestinationAirport());
    assertEquals("HGU", response.getFlights().get(1).getSourceAirport());
    assertEquals("LAE", response.getFlights().get(1).getDestinationAirport());
  }

  @Test
  public void multiplePathsTheShortestIsPreferred() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("LAE");
    request.setDestinationId("WWK");
    TravelRegistrationResponse response = flightController.registerTravel(request);

    assertEquals(2, response.getFlights().size());
    assertEquals("LAE", response.getFlights().get(0).getSourceAirport());
    assertEquals("HGU", response.getFlights().get(0).getDestinationAirport());
    assertEquals("HGU", response.getFlights().get(1).getSourceAirport());
    assertEquals("WWK", response.getFlights().get(1).getDestinationAirport());
    assertEquals(new BigDecimal(540), response.getDistance().setScale(0, RoundingMode.HALF_UP));
  }

  @Test
  public void noPathFoundThrowsBusinessException() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("POM");
    request.setDestinationId("GKA");
    BusinessException thrown = assertThrows(BusinessException.class, () -> flightController.registerTravel(request));
    assertEquals("Travel route not found", thrown.getMessage());
  }

  @Test
  public void noAirportFoundThrowsBusinessException() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("AAA");
    request.setDestinationId("GKA");
    BusinessException thrown = assertThrows(BusinessException.class, () -> flightController.registerTravel(request));
    assertEquals("No airport found for input", thrown.getMessage());
  }

  @Test
  public void updatingAirportChangesDistance() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("GKA");
    request.setDestinationId("IOM");
    TravelRegistrationResponse response = flightController.registerTravel(request);
    assertEquals(4, response.getFlights().size());
    assertEquals(new BigDecimal(14682), response.getDistance().setScale(0, RoundingMode.HALF_UP));


    TravelUpdateRequest updateRequest = new TravelUpdateRequest();
    updateRequest.setOldAirportId("LHR");
    updateRequest.setNewAirportId("LGW");
    TravelRegistrationResponse updateResponse = flightController.updateTravel(response.getTravelId(), updateRequest);
    assertEquals(response.getTravelId(), updateResponse.getTravelId());
    assertEquals(new BigDecimal(14723), updateResponse.getDistance().setScale(0, RoundingMode.HALF_UP));
  }

  @Test
  public void updatingTooDistantAirportIsNotAllowed() {
    TravelRegistrationRequest request = new TravelRegistrationRequest();
    request.setSourceId("GKA");
    request.setDestinationId("IOM");
    TravelRegistrationResponse response = flightController.registerTravel(request);

    TravelUpdateRequest updateRequest = new TravelUpdateRequest();
    updateRequest.setOldAirportId("LHR");
    updateRequest.setNewAirportId("POM");
    BusinessException thrown = assertThrows(BusinessException.class, () -> flightController.updateTravel(response.getTravelId(), updateRequest));
    assertEquals("Distance between replacement airport too big", thrown.getMessage());
  }
}
