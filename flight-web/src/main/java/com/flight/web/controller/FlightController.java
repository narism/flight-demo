package com.flight.web.controller;

import com.flight.domain.api.TravelRegistrationRequest;
import com.flight.domain.api.TravelRegistrationResponse;
import com.flight.domain.api.TravelUpdateRequest;
import com.flight.service.FlightRouteService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/travels")
public class FlightController {

  private FlightRouteService flightRouteService;

  public FlightController(FlightRouteService flightRouteService) {
    this.flightRouteService = flightRouteService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public TravelRegistrationResponse registerTravel(@RequestBody TravelRegistrationRequest request) {
    return flightRouteService.registerTravel(request.getSourceId(), request.getDestinationId());
  }

  @GetMapping("/{travelId}")
  public TravelRegistrationResponse getTravel(@PathVariable UUID travelId) {
    return flightRouteService.getTravelById(travelId);
  }

  @PatchMapping("/{travelId}")
  public TravelRegistrationResponse updateTravel(@PathVariable UUID travelId, @RequestBody TravelUpdateRequest updateRequest) {
    return flightRouteService.updateTravel(travelId, updateRequest);
  }
}