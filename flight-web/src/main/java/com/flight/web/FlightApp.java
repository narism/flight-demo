package com.flight.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.flight")
public class FlightApp {

  public static void main(String[] args) {
    SpringApplication.run(FlightApp.class, args);
  }
}